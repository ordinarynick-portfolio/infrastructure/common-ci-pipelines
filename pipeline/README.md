# Pipeline Folder

This folder contains common pipeline files to simplify the use of GitLab CI pipelines. Each subfolder targets specific
use cases or technologies, allowing for modular and reusable CI/CD configurations.

## List of Files

- **[`gradle`](gradle)**: Contains jobs for building and testing Gradle projects.
- **[`hugo`](hugo)**: Contains jobs for building and deploying Hugo framework projects.
- **[`lint`](lint)**: Contains jobs for linting various types of files to ensure code quality and consistency.
- **[`nodejs`](nodejs)**: Contains jobs for building, testing, and deploying Node.js projects.
- **[`package`](package)**: Contains jobs for creating application packages, such as Docker images.
- **[`release`](release)**: Contains jobs for managing releases and creating tags in GitLab from the pipeline.

Each subfolder is designed to be easily included in your own GitLab CI configuration, promoting a modular approach to
pipeline management.
