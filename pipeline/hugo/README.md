# GitLab CI Jobs for Hugo Framework

This folder contains GitLab CI pipeline jobs specifically designed for the Hugo framework. These jobs streamline the
CI/CD process for Hugo projects by providing essential configurations for building, checking, and deploying.

## List of Files

- **[`base_hugo.yaml`](base_hugo.yaml)**: Base CI pipeline job for the Hugo framework, containing common
  configurations. (All jobs are hidden.)
- **[`smart_hugo_pipeline.yaml`](smart_hugo_pipeline.yaml)**: Comprehensive pipeline job for the Hugo framework,
  including all necessary steps to check and deploy Hugo projects.

Each file is intended to be included in your GitLab CI configuration, promoting efficiency and best practices in
managing Hugo-based projects.
