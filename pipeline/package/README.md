## GitLab CI Jobs for Packages

This folder contains GitLab CI jobs for building and checking application packages, such as Docker images, for services
and libraries. These jobs help automate the creation and validation of deployment artifacts.

### List of Files

- **[`docker_image.yaml`](docker_image.yaml)**: Contains hidden jobs for building and checking Docker images.

Each file is intended to be included in your GitLab CI configuration, promoting efficiency and best practices in
managing application packages.
