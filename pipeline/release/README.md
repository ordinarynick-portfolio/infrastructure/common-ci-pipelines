## GitLab CI Jobs for Releasing

This folder contains GitLab CI jobs for creating releases and tags in GitLab. These jobs help automate the release
process, ensuring consistent and reliable tagging and versioning of your projects.

### List of Files

- **[`merge_request_release.yaml`](merge_request_release.yaml)**: Jobs for creating releases and tagging within merge
  requests.

Each file is designed to be included in your GitLab CI configuration, promoting efficiency and best practices in
managing releases and tags.
