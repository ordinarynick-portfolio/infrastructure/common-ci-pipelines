## Script Folder

This folder contains various scripts that can be used in GitLab CI pipelines to automate and streamline common tasks.

### List of Files

- **[`git_release_description.sh`](git_release_description.sh)**: Extracts the release description from Git commits.
- **[`git_tag_version.sh`](git_tag_version.sh)**: Determines the next tag version based on semantic versioning tagging
  rules.

Each script is designed to be easily integrated into your pipeline, promoting consistency and efficiency in your CI/CD
processes.
